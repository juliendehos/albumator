// g++ -Wall -std=c++11 albumator.cpp  -lboost_filesystem -lboost_system
// ./a.out "Los Angeles, 2010" 20100700_Los-Angeles/ LA2010

#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

using namespace boost::filesystem;
using namespace std;

// some parameters
string thumbSize = "200x200";
string slideSize = "800x600";

int main(int argc, char ** argv) {
  // parse command line
  if (argc != 4) {
    cout << "usage: " << argv[0] << " <title> <input path> <output path>\n"; 
    exit(-1);
  }
	
  // some variables
  string title = argv[1];	
  string inputDir = argv[2];	
  string outputDir = argv[3];	

  // create output directories
  create_directory(path(outputDir));
  create_directory(path(outputDir+"/thumbs"));
  create_directory(path(outputDir+"/slides"));
	
  // create CSS
  ofstream cssFile(outputDir+"/style.css");
  cssFile << "h1 { text-align:center; }\n" << "img { margin:10px; }\n";

  // create album page
  ofstream indexFile(outputDir+"/index.html");
  indexFile << "<html><head>\n" << "<title>" << title << "</title>\n";
  indexFile << "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n";
  indexFile << "</head><body>\n" << "<h1>" << title << "</h1>\n<center>"; 
  // for each input image
  string previous;
  path inputPath(inputDir);
  directory_iterator it(inputPath);
  while (it!=directory_iterator()) {
    // create thumbnails and slide images
    path p = it->path();
    cout << "adding: " << p.string() << endl;
    ostringstream ossSlide;
    ossSlide<<"convert "<<p.string()<<" -resize "<<slideSize<<" "<<outputDir<<"/slides/slide_"<<p.filename();
    system( ossSlide.str().c_str() );
    ostringstream ossThumb;
    ossThumb<<"convert "<<p.string()<<" -resize "<<thumbSize<<" "<<outputDir<<"/thumbs/thumb_"<<p.filename();
    system( ossThumb.str().c_str() );
    // update album page
    indexFile << "<a href=\"slides/slide_" << p.stem().string() << ".html\">";
    indexFile << "<img src=\"thumbs/thumb_" << p.filename().string() << "\"/></a>\n";
    // create slide page
    stringstream ossSlideName;
    ossSlideName << "slide_" << p.stem().string() << ".html";
    string slideName = ossSlideName.str();
    ofstream slideFile( outputDir+"/slides/"+slideName );
    slideFile << "<html><head>\n" << "<title>" << title << "</title>\n";
    slideFile << "<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\" />\n";
    slideFile << "</head><body>\n" << "<title>" << title << "</title>\n";
    slideFile << "<h1>" << title << "</h1><center>\n";
    if (not previous.empty()) slideFile << "<a href=\"" << previous << "\">Previous</a>";
    else slideFile << "Previous";
    previous = slideName;
    slideFile << " - <a href=\"../index.html\">Album</a> - ";
    it++;
    if (it!=directory_iterator()) slideFile << "<a href=\"slide_" << it->path().stem().string() << ".html\">Next</a>";
    else slideFile << "Next";	
    slideFile << "<br><img src=\"slide_" << p.filename().string() << "\"/> \n";
    slideFile << "</center></body></html>\n";	
  }
  indexFile << "</center></body></html>\n";
  return 0;
}
