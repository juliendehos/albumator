#! /usr/bin/env python3
# ./albumator.py "Los Angeles, 2010" 20100700_Los-Angeles/ LA2010

import os
import re
import sys

# parse command line
if len(sys.argv) != 4 :
	print("usage: albumator <title> <input path> <output path>")
	quit()

# some parameters
thumbSize = "200x200"
slideSize = "800x600"

# some variables
title = sys.argv[1]
inputDir = sys.argv[2]
outputDir = sys.argv[3]
images = os.listdir(inputDir)
nbImages = len(images)
stems = [ re.sub("[.](\w*)$", "", i) for i in images]

# create output directories
os.makedirs(outputDir+"/thumbs", exist_ok=True)
os.makedirs(outputDir+"/slides", exist_ok=True)

# create CSS
cssFile = open(outputDir + "/style.css", "w")
cssFile.write("h1 { text-align:center; }\n")
cssFile.write("img { margin:10px; }\n")

# create album page
indexFile = open(outputDir + "/index.html", "w")
indexFile.write("<html><head> <title>"+title+"</title>\n")
indexFile.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n")
indexFile.write("</head><body> <h1>"+title+"</h1>\n")
# for each image
for i in range(0, nbImages) :
	# create thumbnail and slide image
	print("adding: " + images[i])
	os.system("convert "+inputDir+"/"+images[i]+" -resize "+thumbSize+" "+outputDir+"/thumbs/thumb_"+images[i])
	os.system("convert "+inputDir+"/"+images[i]+" -resize "+slideSize+" "+outputDir+"/slides/slide_"+images[i])
	# update index.html
	indexFile.write("<a href=\"slides/slide_"+stems[i]+".html\"><img src=\"thumbs/thumb_"+images[i]+"\"/></a> \n") 
	# create slide page
	slideFile = open(outputDir+"/slides/slide_"+stems[i]+".html", "w")
	slideFile.write("<html><head> <title>"+title+"</title>\n")
	slideFile.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\" />\n")
	slideFile.write("</head><body> <h1>"+title+"</h1><center>\n")
	if i>0:
		slideFile.write("<a href=\"slide_"+stems[i-1]+".html\">Previous</a>\n")
	else:
		slideFile.write("Previous\n")
	slideFile.write(" - <a href=\"../index.html\">Album</a> - \n")
	if i<(nbImages-1):
		slideFile.write("<a href=\"slide_"+stems[i+1]+".html\">Next</a>\n")
	else:
		slideFile.write("Next\n")
	slideFile.write("\n")
	slideFile.write("<br><img src=\"slide_"+images[i]+"\"/> \n")
	slideFile.write("</center></body></html>\n")
indexFile.write("</body></html>\n")
