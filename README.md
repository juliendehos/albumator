# Albumator

HTML photo gallery generator 

## Run

- install ImageMagick 
- see source code...

## Example

### Input 

A directory containing image files. 

### Output

HTML pages: one page with thumbnails + slideshow pages. 

- page with thumbnails:

![](LA2010/albumator_capture_1.png)

- slideshow page:

![](LA2010/albumator_capture_2.png)


