-- runhaskell albumator.hs "Los Angeles, 2010" 20100700_Los-Angeles/ LA2010

import Data.List
import System.Process
import System.Directory
import System.Environment
import System.IO

-- some parameters
sizeThumbs = "200x200"
sizeSlides = "800x600"

-- main function : get arguments and call makeAlbum
main = do
  args <- getArgs
  case args of
    [title, inputPath, outputPath] -> do
      -- create output directories
      createDirectoryIfMissing True outputPath
      createDirectoryIfMissing True (outputPath ++ "/thumbs")
      createDirectoryIfMissing True (outputPath ++ "/slides")
      -- create CSS
      writeFile (outputPath++"/style.css") "h1 {\n  text-align:center;\n}\nimg {\n  margin:10px;\n}\n"
      -- get input image
      dir <- getDirectoryContents inputPath 
      let iFiles = filter (\x -> (x/=".") && (x/="..")) (sort dir)
          indexPath = outputPath ++ "/index.html"
      -- create thumbnails and slide images
      mapM_ (convertImage sizeThumbs inputPath (outputPath ++ "/thumbs/thumb_")) iFiles
      mapM_ (convertImage sizeSlides inputPath (outputPath ++ "/slides/slide_")) iFiles
      -- create slide pages
      mapM_ (createSlidePage title outputPath) $ zip3 iFiles ("":init iFiles) (tail iFiles ++ [""])
      -- create main page
      writeFile indexPath $ "<html><head> \n<title>"++title++"</title>\n"
      appendFile indexPath "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" /> \n</head>\n"
      appendFile indexPath $ "<body>\n<h1>"++title++"</h1><center> \n"
      appendFile indexPath $ foldl (\acc x -> acc ++ "\n <a href=\"slides/slide_" ++ imageToWeb x ++ "\"> "
                                             ++"<img src=\"thumbs/thumb_" ++ x ++ "\"/></a>" ) [] iFiles
      appendFile indexPath "\n</center></body></html>\n"
    -- invalid args
    _ -> putStrLn "Usage: albumator <title> <input path> <output path>"

-- resize an image using convert (ImageMagick)
convertImage size iPath oPrefix iFile = do
  system $ "convert -resize " ++ size ++ " " ++ iPath ++ "/" ++ iFile ++ " " ++ oPrefix ++ iFile
  putStrLn $ "creating " ++ oPrefix ++ iFile

-- create a slide page
createSlidePage title outputPath (image, previous, next) = do
  let filename = outputPath++ "/slides/slide_" ++ imageToWeb image
  writeFile filename $ "<html>\n<head> \n<title>"++title++"</title>\n"
  appendFile filename "<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\" />\n"
  appendFile filename $ "</head>\n<body> \n<h1>"++title++"</h1>\n<center> \n"
  appendFile filename $ formatLink previous "Previous"
  appendFile filename " - <a href=\"../index.html\">Album</a> - "
  appendFile filename $ formatLink next "Next"
  appendFile filename $ "<br><img src=\"slide_"++ image ++ "\"/>\n</center>\n</body>\n</html>\n"

-- replace extension by ".html"
imageToWeb image = dropWhileEnd (/='.') image ++ "html"

-- format html link if relevant
formatLink link text = case link of
  "" -> text
  _ -> "<a href=\"slide_"++ imageToWeb link ++ "\">" ++ text ++ "</a>"
