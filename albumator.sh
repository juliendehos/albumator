#! /bin/bash
# ./albumator.sh "Los Angeles, 2010" 20100700_Los-Angeles/ LA2010

# parse command line 
if [ $# != 3 ] 
then
    echo "usage: albumator <title> <input path> <output path>"
    exit
fi

# some parameters
thumb_size=200x200
slide_size=800x600

# some variables
title=$1
input_dir=$2
output_dir=$3
images=`find $input_dir -name "*.*" | sed -E 's/^[0-9a-zA-Z/_-]*[/]//'`

nb=0
for i in $images
do
	nb=`expr $nb + 1`
	names[$nb]=$i
	stems[$nb]=`echo ${names[$nb]} | sed -E 's/[.][0-9a-zA-Z/_-]*$//'`
done

# create output directories
mkdir -p $output_dir/thumbs
mkdir -p $output_dir/slides

# create CSS
css_file=$output_dir/style.css
echo "h1 { text-align:center; }" > $css_file
echo "img { margin:10px; }" >> $css_file

# create album page
index_file=$output_dir/index.html
echo "<html><head> <title>$title</title>" > $index_file
echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />" >> $index_file
echo "</head><body> <h1>$title</h1>" >> $index_file
# for each image
for (( n=1; n<=$nb; n++ )) 
do
	# create thumbnail and slide image
	echo "adding: ${names[$n]}"
	convert $input_dir/${names[$n]} -resize $thumb_size $output_dir/thumbs/thumb_${names[$n]}
	convert $input_dir/${names[$n]} -resize $slide_size $output_dir/slides/slide_${names[$n]}
	# update index file
	echo "<a href=\"slides/slide_${stems[$n]}.html\"><img src=\"thumbs/thumb_${names[$n]}\"/></a> " >> $index_file
	# create slide page
	slide_file=$output_dir/slides/slide_${stems[$n]}.html
	echo "<html><head> <title>$title</title>" > $slide_file
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\" />" >> $slide_file
	echo "</head><body> <h1>$title</h1><center>" >> $slide_file
	if [ $n -gt 1 ]
	then
		i=`expr $n - 1`
		echo "<a href=\"slide_${stems[$i]}.html\">Previous</a>" >> $slide_file
	else
		echo "Previous" >> $slide_file
	fi
	echo " - <a href=\"../index.html\">Album</a> - " >> $slide_file
	if [ $n -lt $nb ]
	then
		i=`expr $n + 1`
		echo "<a href=\"slide_${stems[$i]}.html\">Next</a>" >> $slide_file
	else
		echo "Next" >> $slide_file
	fi
	echo " <br><img src=\"slide_${names[$n]}\"/> " >> $slide_file
	echo "</center></body></html>" >> $slide_file
done
echo "</body></html>" >> $index_file
