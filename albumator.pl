#! /usr/bin/perl
# ./albumator.pl "Los Angeles, 2010" 20100700_Los-Angeles/ LA2010

use IO::File;

# parse command line
die "usage: albumator <title> <input path> <output path>" if @ARGV != 3;
my ($title, $inputDir, $outputDir) = @ARGV; 

# some parameters
my $thumbSize = "200x200";
my $slideSize = "800x600";

# some variables
chomp(my @images = sort(`find $inputDir -name "*.*" | sed -E 's/^[0-9a-zA-Z/_-]*[/]//'`));
chomp(my @stems = sort(`find $inputDir -name "*.*" | sed -E 's/^[0-9a-zA-Z/_-]*[/]//' | sed -E 's/[.][0-9a-zA-Z/_-]*\$//' `));

# create output directories
mkdir($outputDir);
mkdir($outputDir."/slides");
mkdir($outputDir."/thumbs");

# create CSS
my $cssFile = IO::File->new($outputDir."/style.css", ">");
$cssFile->print("h1 { text-align:center; }\n");
$cssFile->print("img { margin:10px; }\n");

# create album page
my $indexFile = IO::File->new($outputDir."/index.html", ">");
$indexFile->print("<html><head> <title>$title</title>\n");
$indexFile->print("<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n");
$indexFile->print("</head><body> <h1>$title</h1>\n");
# for each image
foreach my $i (0..@images-1) {
	# create thumbnail and slide image
	print("adding: ${images[$i]}\n");	
	system("convert $inputDir/${images[$i]} -resize $thumbSize $outputDir/thumbs/thumb_${images[$i]}");
	system("convert $inputDir/${images[$i]} -resize $slideSize $outputDir/slides/slide_${images[$i]}");
	# update index.html
	$indexFile->print("<a href=\"slides/slide_${stems[$i]}.html\"><img src=\"thumbs/thumb_${images[$i]}\"/></a>\n");
	# create slide page
	my $slideFile = IO::File->new($outputDir."/slides/slide_${stems[$i]}.html", ">");
	$slideFile->print("<html><head> <title>$title</title>\n");
	$slideFile->print("<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\" />\n");
	$slideFile->print("</head><body> <h1>$title</h1><center>\n");
	if ($i != 0) { $slideFile->print("<a href=\"slide_${stems[$i-1]}.html\">Previous</a>\n"); }
	else { $slideFile->print("Previous\n"); }
	$slideFile->print(" - <a href=\"../index.html\">Album</a> - \n");
	if ($i != @images-1) { $slideFile->print("<a href=\"slide_${stems[$i+1]}.html\">Next</a>\n"); }
	else { $slideFile->print("Next\n"); }
	$slideFile->print(" <br><img src=\"slide_${images[$i]}\"/>\n");
	$slideFile->print("</center></body></html>\n");
}
$indexFile->print("</body></html>\n");
